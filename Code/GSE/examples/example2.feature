Feature: Turn on the irrigator 2
Description:
    As an gardener
    Or the owner of the house
    I want to select when the irrigator will turn on
    So that I can make sure that the plants will recieve the correct amount of water

Group:
    Automation, Garden Care

Constraints and Qualities:
    The requirement should implement:
    * An nice green button to turn the action on
    The actor should not be able to:
    * I should not be able to override a command given by a home owner
        > he can't be overriden
    * To shut down the irrigator

Relationship:
    This requirement is:
    * Derived from requirement(s) req 1, req2, req 1 2, req 3
    * Contained in requirement(s) req a, req2, req 1 2, req 3
    * A copy of the requirement(s) req c, req2, req 1 2, req 3
    * A refinament of the requirement(s) req f, req2, req 1 2, req 3

Planning:
    This requirement will be implemented 1st and will take 60 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Mariah Cash, Josh Puddle. The sprint that will implement this feature is: 3.

Metrics:
    The metrics used to evaluate this requirement are:
    * NOL;
    * NOW;
    * NOH;
    * CMPLX; The expected value is at least 50 lines
    * SOMETHING; The expected value is at least 50 whatever it is
    * KLS;
    * JKHST; The expected value is at least 3948534958 units

Notes:
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Nam efficitur odio purus, a congue turpis tincidunt rutrum. 
    Suspendisse congue faucibus enim eu vestibulum. 
    Praesent ex neque, interdum sollicitudin libero eget, aliquam placerat sapien. 
    Donec vel dui faucibus, hendrerit quam vitae, fringilla ex. 
    Quisque eu lacus tempor, finibus augue nec, laoreet est. 
    : > / Maecenas in dignissim ipsum. 
    



    