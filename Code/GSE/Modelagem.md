# Cada arquivo gera
```
Description(
    Lista de STAKEHOLDERS
    1 ACTION
    1 MOTIVATION
)
Group(
    Lista de TAGS
)
Constraints(
    Lista de QUALIDADES -> Pode ter lista de Subitens
    Lista de RESTRIÇÕES -> Pode ter lista de Subitens
)
Relationship(
    5 Listas{
        DERIVED
        CONTAINS
        COPY
        REFINAMENT
        DEPENDENT
    }
)
Planning(
    1 ORDER
    1 TIME_NEEDED
    1 NUM_OF_DEVELOPERS
    Lista de DEVELOPERS 
    1 SPRINT
)
Metrics(
    Lista de METRIC 
    lista de EXPECTED_VALUE
)
Notes(
    String (lista de words)
)
```