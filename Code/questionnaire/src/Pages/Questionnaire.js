import React, { Component } from 'react';
import {Button, Card, Col, Row} from 'react-materialize'
import InputText from '../Components/InputText'
import {t, capitalize} from '../environment'

class Questionnaire extends Component {
  constructor(props){
    super(props);
    this.state={
      lang: '',
    }
    
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.lang !== this.props.lang){
      this.setState({lang: this.props.lang})
    }
  }
  componentWillMount(){
    this.setState({lang: this.props.lang})
  }
  render() {
    return (
      <div className="questionnaire-root">
        <Row>
          <Col s={12}>
            <Card title={t('description', this.state.lang).toUpperCase()}>
              <Row>
                <Col s={4}>

                  <InputText 
                    id={'stakeholder'}
                    type={'text'}
                    mode={'normal'}
                    text={capitalize(t('actor', this.state.lang))}
                    color={'#ef6c00'}
                  />
                </Col>
                <Col s={4}>
                  <InputText 
                    id={'action'}
                    type={'text'}
                    mode={'normal'}
                    text={capitalize(t('actor', this.state.lang))}
                    color={'#ef6c00'}
                  />
                </Col>
                <Col s={4}>
                  <InputText 
                    id={'motivation'}
                    type={'text'}
                    mode={'normal'}
                    text={capitalize(t('actor', this.state.lang))}
                    color={'#ef6c00'}
                  />
                </Col>
                <Button>{t('finish_requirement', this.state.lang).toUpperCase()}</Button>

              </Row>
            </Card>
          </Col>
          <Col s={12}>
            <Card title={t('group', this.state.lang).toUpperCase()}>
              CONTEÚDO
            </Card>
          </Col>
          <Col s={12}>
            <Card title={t('constraints_and_qualities', this.state.lang).toUpperCase()}>
              CONTEÚDO
            </Card>
          </Col>
          <Col s={12}>
            <Card title={t('relationship', this.state.lang).toUpperCase()}>
              CONTEÚDO
            </Card>
          </Col>
          <Col s={12}>
            <Card title={t('planning', this.state.lang).toUpperCase()}>
              CONTEÚDO
            </Card>
          </Col>
          <Col s={12}>
            <Card title={t('metrics', this.state.lang).toUpperCase()}>
              CONTEÚDO
            </Card>
          </Col>
          <Col s={12}>
            <Card title={t('notes', this.state.lang).toUpperCase()}>
              CONTEÚDO
            </Card>
          </Col>          
        </Row>
        <Button>{t('finish_requirement', this.state.lang).toUpperCase()}</Button>
      </div>
    );
  }
}

export default Questionnaire;
