import React, { Component } from 'react';

class InputText extends Component { 
  
  constructor(props){
    super(props);    
    this.changeStyle = this.changeStyle.bind(this)
    this.state ={
      focus:false,
      style:{}
    }
  }
  componentWillMount(){
    var defaultColor = ''
    defaultColor = this.props.color ? this.props.color : '#78752e'
    let style = {
      contrast:{
        input:{borderColor:"yellow"},
        input_focus:{borderColor:"yellow", boxShadow:"0 1px 0 0 yellow"},
        label_focus:{color: 'yellow'},
        label:{color:'white'}
      },
      normal:{
        input:{borderColor: `${defaultColor}` },
        input_focus:{borderColor: `${defaultColor}`, boxShadow:`0 1px 0 0 ${defaultColor}` },
        label_focus:{color: `${defaultColor}`},
        label:{color:'black'}
      }
    }
    this.setState({style:style})
    if(this.props.mode === 'contrast'){
      this.setState({mode: 'contrast'})
    }
    else{
      this.setState({mode: 'normal'})  
    }    
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.mode !== this.props.mode){
      if(nextProps.mode === 'contrast'){
        this.setState({mode: 'contrast'})
      }
      else{
        this.setState({mode: 'normal'})  
      }
    }
  }
  render() {
    let style = this.state.style 
    if(this.props.type !== 'textarea'){
      return (
        <div className="input-field">
          <input
            placeholder={this.props.placeholder} 
            id={this.props.id} 
            type={this.props.type} 
            className={this.props.classes}
            style={
              !this.state.focus?
              style[this.state.mode].input
              :
              style[this.state.mode].input_focus
            }
            value={this.props.value}
            onFocus={this.changeStyle}
            onBlur={this.changeStyle}
          />
          <label 
            htmlFor={this.props.id} 
            style={
              !this.state.focus?
              style[this.state.mode].label
              :
              style[this.state.mode].label_focus
            }
          >
            {this.props.text}
          </label>
        </div>
      )
    }
      else{
        return (
          <div className="input-field">            
            <textarea 
            placeholder={this.props.placeholder} 
            id={this.props.id} 
            className={"materialize-textarea " + this.props.classes}
            style={
              !this.state.focus?
              style[this.state.mode].input
              :
              style[this.state.mode].input_focus
            }
            onFocus={this.changeStyle}
            onBlur={this.changeStyle}
            ></textarea>
            <label 
              htmlFor={this.props.id} 
              style={
                !this.state.focus?
                style[this.state.mode].label
                :
                style[this.state.mode].label_focus
              }
            >
              {this.props.text}
            </label>
          
          </div>
        );
      }
  }
  changeStyle(){
    let current_focus = !this.state.focus;
    this.setState({focus: current_focus})
  }
}

export default InputText;
