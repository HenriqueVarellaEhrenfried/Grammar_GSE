const language={
    'default': 'en_US',
    'en_US':{
        'description': 'description',
        'group': 'group',
        'constraints_and_qualities': 'constraints and qualities',
        'relationship':'relationship',
        'planning': 'planning',
        'metrics': 'metrics',
        'notes': 'notes',
        'finish_requirement':'finish requirement',
        'actor':'actor',
        'motivation': 'motivation',
        'action': 'action'
    }
}

export default language;