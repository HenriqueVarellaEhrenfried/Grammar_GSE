import language from './i18n'

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

export function t(sentence, lang){
    return language[lang][sentence];
}

export function capitalize(string){
	return string.capitalize()
}