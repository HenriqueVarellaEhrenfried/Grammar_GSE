import React, { Component } from 'react';
import './App.css';
import Questionnaire from './Pages/Questionnaire';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Questionnaire lang={'en_US'}/>
      </div>
    );
  }
}

export default App;
