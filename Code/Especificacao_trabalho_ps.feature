Constraints and Qualities:
    The requirement should implement:

    The actor should not be able to:


Feature: Show PerfectCode history
Description:
	As a student
	I want to discover the history of the game
    So that I can get motivated to play the game

Group: Introduction

Scenario: Correctly display the game's story
Given I opened the game
When the game loads
Then it will print the story of the game

-------------------------------------------------------------------

Feature: Open a door
Description:
	As a student
	I want to open a door
    So that I can get the next challenge

Group: Game mechanics

Constraints and Qualities:
    The requirement should implement:
    * A validation mechanism that compares the output of the solution provided by the user with the output expected 
    The actor should not be able to:
    * Open a door if the solution is incorrect
    * Retry to solve the problem if it exceeds three plus the number of cards that gives bonus chances

Relationship:
    This requirement is:
    * Dependent on requirement(s) Set up the game

Scenario: Try to open a door with a correct solution
Given a door is presented
And a coding problem is proposed
And I find a correct solution for the problem
When submit my solution
Then the game will compile my solution
And the game will run the solution with a test case
And the game will compare the output of my solution with a correct output
And the door will be opened
And I can choose the card that gives me one more chance to open a door or the card that doubles the time to kill a monster

Scenario: Try to open a door with an incorrect solution
Given a door is presented
And a coding problem is proposed
And I find a incorrect solution for the problem
When submit my solution
Then the game will compile my solution
And the game will run the solution with a test case
And the game will compare the output of my solution with a correct output
And the door will not be opened
And the game will ask to player to try again the same problem
And the player will lose a life point if the it tried three plus the number of cards that gives bonus chances times to solve the problem
And the game will start over if the player tried three plus the number of cards that gives bonus chances times to solve the problem

-------------------------------------------------------------------

Feature: Enter through a door
Description:
	As a student
	I want to enter through a door
    So that I can progress in the game

Group: Game mechanics

Constraints and Qualities:
    The requirement should implement:
    * A validation method to ensure the line selected by the player is the wrong line
    The actor should not be able to:
    * Kill the monster if it selects the wrong line

Relationship:
    This requirement is:
    * Dependent on requirement(s) Open a door, Set up the game

Scenario: Enter a door without a monster
Given a door is opened
And there is no COBOLORC
When I enter through the door
Then I will be presented to the next door

Scenario: Enter a door and kill the monster successfully
Given a door is opened
And there is a COBOLORC
And a code with error is presented
When I submit a the line with error correctly within the established time
Then the game will verify if the inserted line is correct
And I will kill the monster
And I will enter the door
And I will be presented to the next door

Scenario: Enter a door and do not answer within the established time
Given a door is opened
And there is a COBOLORC
And a code with error is presented
When the established time is over
Then I will go back to the begining of the game
And I will lose a life point

Scenario: Enter a door and do not answer the correct line
Given a door is opened
And there is a COBOLORC
And a code with error is presented
When I submit a the line with error within the established time
Then the game will verify if the inserted line is correct
And I will go back to the begining of the game
And I will lose a life point
-------------------------------------------------------------------

Feature: Set up the game
Description:
	As a professor
	I want to set the game form my students
    So that I can make sure the game will help my students 

Group: Game adminsitration

Constraints and Qualities:
    The requirement should implement:
    * A simple form that allow the professor to add:
        > The number of doors
        > The problem definition of each door 
        > The valid test case for each door
        > The number of monsters
        > The problem of each monster 
        > The answer of each monster's problem 
        > The time to solve the monster's problem
    * A random selector to choose which doors have monsters 

Scenario: Set up the game correctly
Given I am setting the game
And I added the number of doors
And I added the problems for each door
And I added the the number of monsters
And I added the problems of the monsters 
And I added the answer of the problem of the monster 
And I added the time to solve the monster's problem
When I submit the settings
Then the game will be configured

Scenario: Set up the game incorrectly
Given I am setting the game
And I do not fill a required field
When I submit the settings
Then the game will not be configured
And a message informing whats wrong will be displayed
-------------------------------------------------------------------

Feature: View the status
Description:
	As a student
	I want to view my progress and the cards I own
    So that I can know if I am beating the game and plan what card I should pick next

Group: UI

Constraints and Qualities:
    The actor should not be able to:
    * Directly manipulate the status bar

Relationship:
    This requirement is:
    * Dependent on requirement(s) Open a door, Enter through a door

Scenario: Display the status
Given I am playing the game
When look to the status bar
Then I see the the number of problems I solved
And I see the number of tries for each problem I spent
And I see the cards I own


