# Controle Acadêmico

## Especificação
Considere que deve ser criado um sistema que auxilie o controle acadêmico, devendo ter funcionalidades que dêem suporte à seis principais módulos: Matriculator, Ajustinator, Atividades Formativas, Estágios, Visualizator e Pedidos. 

O Matriculator ​deve permitir a um aluno o pedido de matrícula em determinadas matérias. 

O Ajustinator permitirá que o aluno faça os pedidos de reajuste de matricula no período pertinente, sem que hajam pedidos de reajuste de matrículas para o mesmo horário e dia.

 O módulo de Atividades Formativas deve permitir a inserção de documentos que comprovem a realização de atividades formativas por parte do aluno, de modo que eles fiquem armazenados aguardando a validação por parte da coordenação a partir do momento que o pedido de validação for feito. 
 
A parte de Estágios ​deve permitir que o aluno preencha os dados necessários os formulários de Documento Informativo para Realização de Estágio em Empresa e do Termo de Compromisso que serão gerados pelo sistema, e uma vez gerados aguardaram a validação por parte dos professores responsáveis e da COE (Comissão Orientadora de Estágios).

 O Visualizator ​irá mostrar para o aluno um desenho da grade curricular do seu curso, onde cada disciplina estará pintada de uma cor que corresponde à sua situação naquela disciplina (aprovado, reprovado, matriculado, equivalência e não cursado). As informações de nota e frequência das vezes em que a disciplina foi cursada devem estar disponíveis para visualização quando uma disciplina for selecionada.

 A parte de Pedidos ​deverá permitir que o aluno preencha formulários de pedidos que deverão ser avaliados pela coordenação de seu curso, sendo possível ver o status do pedido no sistema.

## Requisitos
- [Feito] Permitir aluno pedir matrícula em disciplina
- [Feito] Permitir aluno pedir ajuste de matrícula
- [Feito] Permitir a inserção de documentos que comprovem a realização de atividades formativas
- [Feito] Permitir o aluno preencha os documentos para estágio
- [Feito] Mostrar a grade curricular
- [Feito] Mostrar status (notas e frequência) nas disciplinas cursadas
- [Feito] Permitir aluno a preencher formulários de pedidos para a coordenação
- [Feito] Exibir o status dos pedidos

## Uso da linguagem
### Modelo
```gherkin
Feature:
Description:
Group:
Constraints and Qualities:
Relationship:
Planning:
Metrics:
Notes:
```
### Implementação do Sistema
```gherkin
Feature: Allow student register application
Description:
	As a student 
	I want to register my application
	So that I can study in the following period 
Group: Matriculator
Constraints and Qualities:
	-> The student can register an application for disciplines if
		> A discplines was not taken
		> The new discpline does not have schedule conflict
		> All prerequisites are fulfilled
Planning:
	Time needed: 20 hours
	Number of developers: 1
	Assigned to: Mariah Cash
	Sprint: 1
```

```gherkin
Feature: Allow student adjust application
Description:
	As a student 
	I want to adjust my application
	So that I can optimize the disciplines I will take 
Group: Ajustinator
Constraints and Qualities:
	-> The student can adjust an application for disciplines if
		> A discplines was not taken
		> The new discpline does not have schedule conflict
		> All prerequisites are fulfilled
		> The student will cancel the discipline
Planning:
	Time needed: 20 hours
	Number of developers: 1
	Assigned to: Mariah Cash
	Sprint: 2
```
```gherkin
Feature: Allow student upload voucher of formative activity
Description:
	As a student 
	I want to prove that I made a formative activity
	So that I can win the needed credits to graduate 
Group: (Formative Activity)
Constraints and Qualities:
	-> Before accpet the voucher, a validation must be done by the course coordinator
Planning:
	Time needed: 16 hours
	Number of developers: 1
	Assigned to: Josh Von Neuer
	Sprint: 1
```
```gherkin
Feature: Allow student fill intership documents
Description:
	As a student 
	I want to fill my intership documents
	So that I can formalize my intership
Group: (Intership)
Constraints and Qualities:
	-> After filled, these documents have to be validated by COE (Comissão Orientadora de Estágios)
Planning:
	Time needed: 8 hours
	Number of developers: 1
	Assigned to: Josh Von Neuer
	Sprint: 2
```

```gherkin
Feature: Show curricular grade
Description:
	As a student 
	I want to view my status in course
	So that I can organize my activities for the next period
Group: Visualizator
Constraints and Qualities:
	-> The disciplines will be painted:
		> Green: if the student is approved in the discipline		
		> Red: if the student is repproved in the discipline 
		> Blue: if the student is enrolled in the discipline
		> Orange: if the student did not enrolled the discipline
Planning:
	Time needed: 12 hours
	Number of developers: 1
	Assigned to: Mario Ferraruci
	Sprint: 1
```
```gherkin
Feature: Show discipline status
Description:
	As a student 
	I want to view my status in a discipline
	So that I can know which disciplines I need to work harder
Group: Visualizator
Constraints and Qualities:
	-> The status should be shown if the discipline was selected and coursed
	-> The informations that must appear are: grade and frequency
Relationship:
	Depends on 'Show curricular grade'
Planning:
	Time needed: 12 hours
	Number of developers: 1
	Assigned to: Mariah Cash
	Sprint: 3
```
```gherkin
Feature: Allow student's request
Description:
	As a student 
	I want to make a request
	So that I can improve my academic life
Group: Request
Constraints and Qualities:
	-> All requests must be sent to the course coordinator
Planning:
	Time needed: 6 hours
	Number of developers: 1
	Assigned to: Mariah Cash
	Sprint: 3
```
```gherkin
Feature: Display request status
Description:
	As a student 
	I want to watch my request
	So that I can know is happening with my request
Group: Request
Relationship:
	Depends on "Allow student's request"
Planning:
	Time needed: 6 hours
	Number of developers: 1
	Assigned to: Mariah Cash
	Sprint: 3
```

## Notas
- Pode ser útil definir um campo chamado `Implementation Order` no Planning para que o seja possível criar um Grafo de PERT-CPM mais preciso. Este exemplo mostra bem que não dá para confiar apenas no `Depends on`.  