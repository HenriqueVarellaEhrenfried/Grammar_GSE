# Controle Acadêmico

## Especificação
Considere que deve ser criado um sistema que auxilie o controle acadêmico, devendo ter funcionalidades que dêem suporte à seis principais módulos: Matriculator, Ajustinator, Atividades Formativas, Estágios, Visualizator e Pedidos. 

O Matriculator ​deve permitir a um aluno o pedido de matrícula em determinadas matérias. 

O Ajustinator permitirá que o aluno faça os pedidos de reajuste de matricula no período pertinente, sem que hajam pedidos de reajuste de matrículas para o mesmo horário e dia.

 O módulo de Atividades Formativas deve permitir a inserção de documentos que comprovem a realização de atividades formativas por parte do aluno, de modo que eles fiquem armazenados aguardando a validação por parte da coordenação a partir do momento que o pedido de validação for feito. 
 
A parte de Estágios ​deve permitir que o aluno preencha os dados necessários os formulários de Documento Informativo para Realização de Estágio em Empresa e do Termo de Compromisso que serão gerados pelo sistema, e uma vez gerados aguardaram a validação por parte dos professores responsáveis e da COE (Comissão Orientadora de Estágios).

 O Visualizator ​irá mostrar para o aluno um desenho da grade curricular do seu curso, onde cada disciplina estará pintada de uma cor que corresponde à sua situação naquela disciplina (aprovado, reprovado, matriculado, equivalência e não cursado). As informações de nota e frequência das vezes em que a disciplina foi cursada devem estar disponíveis para visualização quando uma disciplina for selecionada.

 A parte de Pedidos ​deverá permitir que o aluno preencha formulários de pedidos que deverão ser avaliados pela coordenação de seu curso, sendo possível ver o status do pedido no sistema.

## Requisitos
- [Feito] Permitir aluno pedir matrícula em disciplina
- [Feito] Permitir aluno pedir ajuste de matrícula
- [Feito] Permitir a inserção de documentos que comprovem a realização de atividades formativas
- [Feito] Permitir o aluno preencha os documentos para estágio
- [Feito] Mostrar a grade curricular
- [Feito] Mostrar status (notas e frequência) nas disciplinas cursadas
- [Feito] Permitir aluno a preencher formulários de pedidos para a coordenação
- [Feito] Exibir o status dos pedidos

## Uso da linguagem
### Modelo
```gherkin
Feature:
Description:
Group:
Constraints and Qualities:
Relationship:
Planning:
Metrics:
Notes:
```
### Implementação do Sistema
```gherkin
Feature: Allow student register application
Description:
	As a student 
	I want to register my application
	So that I can study in the following period 
Group: Matriculator
Constraints and Qualities:
	I should not be able to:
	* Register an application if:
		> A discipline was already taken
		> The new discipline has schedule conflict
		> Any prerequisite is not fulfilled
Planning:
	This requirement will be implemented 1st and will take 20 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Mariah Cash in the sprint 1.

```

```gherkin
Feature: Allow student adjust application
Description:
	As a student 
	I want to adjust my application
	So that I can optimize the disciplines I will take 
Group: Ajustinator
Constraints and Qualities:
	The requirement should implement:
	* A way to allow the student to cancel a discipline
	I should not be able to:
	* Adjust an aplication for disciplines if:
		> The discipline was already taken
		> The new discipline schedule's conflicts with the student schedule
		> Any prerequisite is not fulfilled 

Planning:
	This requirement will be implemented 3rd and will take 20 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Mariah Cash in the sprint 2. 

```
```gherkin
Feature: Allow student upload voucher of formative activity
Description:
	As a student 
	I want to prove that I made a formative activity
	So that I can win the needed credits to graduate 
Group: (Formative Activity)
Constraints and Qualities:
	The requirement should implement:
	* A way to allow students to upload their vouchers to prove their formative activities
	I should not be able to:
	* Register the formative activities without the validation of the course coordinator

Planning:
	This requirement will be implemented 2nd and will take 16 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Josh Von Neuer in the sprint 1. 

```
```gherkin
Feature: Allow student fill intership documents
Description:
	As a student 
	I want to fill my intership documents
	So that I can formalize my intership
Group: (Intership)
Constraints and Qualities:
	The requirement should implement:
	* A way to upload intership documents to COE
	I should not be able to:
	* Register the intership without the validation of COE (Comissão Orientadora de Estágios)
Planning:
	This requirement will be implemented 4th and will take 8 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Josh Von Neuer in the sprint 2. 

```

```gherkin
Feature: Show curricular grade
Description:
	As a student 
	I want to view my status in course
	So that I can organize my activities for the next period
Group: Visualizator
Constraints and Qualities:
	The requirement should implment:
	* A mechanism that colors the discipline according to its status:
		> Green: if the student is approved in the discipline		
		> Red: if the student is repproved in the discipline 
		> Blue: if the student is enrolled in the discipline
		> Orange: if the student did not enrolled the discipline 

Planning:
	This requirement will be implemented 5th and will take 12 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Mario Ferraruci in the sprint 1. 

```
```gherkin
Feature: Show discipline status
Description:
	As a student 
	I want to view my status in a discipline
	So that I can know which disciplines I need to work harder
Group: Visualizator
Constraints and Qualities:
	The requirement should implement:
	* Show, when selected, the information about a coursed discipline. The informations that need to be shown are grande and frequency

Relationship:
	This requirement is:
	* Dependent on requirement(s) 'Show curricular grade'
Planning:
	This requirement will be implemented 6th and will take 12 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Mariah Cash in the sprint 3. 

```
```gherkin
Feature: Allow student's request
Description:
	As a student 
	I want to make a request
	So that I can improve my academic life
Group: Request
Constraints and Qualities:
	The requirement should implement:
	* A way to send requests to the course coordinator
Planning:
	This requirement will be implemented 7th and will take 6 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Mariah Cash in the sprint 3. 

```
```gherkin
Feature: Display request status
Description:
	As a student 
	I want to watch my request
	So that I can know is happening with my request
Group: Request
Relationship:
	This requirement is:
	* Dependent on requirements(s): "Allow student's request"
Planning:
	This requirement will be implemented 8th and will take 6 hours to be implemented by 1 developer(s). The developer(s) responsible for implementing this requirement is/are Mariah Cash in the sprint 3. 
```

## Notas
- Talvez trovar a primeira pessoa do campo de Constraints and Qualities: `I should not be able to:` para  `[USER/ACTOR] should not be able to:` .  

