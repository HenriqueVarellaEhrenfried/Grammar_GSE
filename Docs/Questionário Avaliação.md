## O SUS (System Usability Model)

 O SUS (US Gov) utiliza um questionário com dez perguntas:

**1)** I think that I would like to use this system frequently.

**2)** I found the system unnecessarily complex.

**3)** I thought the system was easy to use.

**4)** I think that I would need the support of a technical person to be able to use this system.

**5)** I found the various functions in this system were well integrated.

**6)** I thought there was too much inconsistency in this system.

**7)** I would imagine that most people would learn to use this system very quickly.

**8)** I found the system very cumbersome to use.

**9)** I felt very confident using the system.

**10)** I needed to learn a lot of things before I could get going with this system.


Cada pergunta pode ser respondida com um valor entre 1 e 5.

O SUS resulta em um valor único geral para o sistema, mas para analisar oa linguagem será preciso analisar do ponto de vista de leitura de algo especificado e de escrita de uma especificação. Para tanto é possível criar uma variação deste questionário contendo 30 questões, 10 para avaliar a linguagem como um todo, 10 para valiar a escrita de algo, com a linguagem e 10 para avaliar a leitura de algo escrito na linguage. Com estes valores seria possível avaliar o sistema sob três ângulos diferentes.

Em português as peguntas do SUS podem ser escritas como:

**1)** Eu acho que eu gostaria de usar este sistema frequentemente

**2)** Eu achei este sistema desnecessariamente complexo

**3)** Eu pensei que o sistema foi fácil de usar

**4)** Eu acho que eu precisaria da ajuda de uma pessoa técnica para ser capaz de usar este sistema

**5)** Eu achei que as várias funcionalidades neste sistema estavam bem integradas

**6)** Eu pensei que havia muita inconsistência neste sistema

**7)** Eu imagino que muitas pessoas aprenderiam a usar este sistema bem rapidamente

**8)** Eu achei o sistema muito complicado de usar

**9)** Eu me senti muito confiante usando o sistema

**10)** Eu preciso aprender muitas coisas antes que eu possa usar o sistema


Para avaliar a linugagem será criado um questionário baseado no SUS.


## Novo questionário

### Quanto a linguagem como um todo

**1)** Eu acho que eu gostaria de usar esta linguagem quando for especificar um sistema

**2)** Eu achei esta linguagem desnecessariamente complexa

**3)** Eu pensei que a linguagem foi fácil de ler e escrever

**4)** Eu acho que eu precisaria da ajuda de uma pessoa técnica para ser capaz de usar esta linguagem

**5)** Eu achei que as várias características desta linguagem estavam bem integradas

**6)** Eu pensei que havia muita inconsistência nesta linguagem

**7)** Eu imagino que muitas pessoas aprenderiam a usar esta linguagem bem rapidamente

**8)** Eu achei a linguagem muito complicada de ler e escrever

**9)** Eu me senti muito confiante usando a linguagem

**10)** Eu preciso aprender muitas coisas antes que eu possa usar esta linguagem


### Quanto a linguagem do ponto de vista de leitura de um documento escrito com a linguagem

**1)** Eu acho que eu gostaria de ler mais especificações escritas com esta linguagem

**2)** Eu achei esta linguagem desnecessariamente complexa de ler

**3)** Eu pensei que a linguagem foi fácil de ler

**4)** Eu acho que eu precisaria da ajuda de uma pessoa técnica para ser capaz de entender uma especificação escrita com esta linguagem

**5)** Eu achei que a linguagem está bem coesa para leitura

**6)** Eu pensei que havia muita inconsistência nesta linguagem quando li uma especificação escrita com ela

**7)** Eu imagino que muitas pessoas aprenderiam a ler especificações nesta linguagem bem rapidamente

**8)** Eu achei a linguagem muito complicada de ler

**9)** Eu me senti muito confiante lendo especificações nesta linguagem

**10)** Eu preciso aprender muitas coisas antes que eu possa entender algo escrito nesta linguagem


### Quanto a linguagem do ponto de vista de escrita de um documento com a linguagem

**1)** Eu acho que eu gostaria de escrever mais especificações com esta linguagem

**2)** Eu achei esta linguagem desnecessariamente complexa de escrever

**3)** Eu pensei que a linguagem foi fácil de escrever

**4)** Eu acho que eu precisaria da ajuda de uma pessoa técnica para ser capaz de escrever uma especificação com esta linguagem

**5)** Eu achei que a linguagem está bem natural de escrever

**6)** Eu pensei que havia muitos problemas de consistência que precisei contornar quando especifiquei com esta linguagem

**7)** Eu imagino que muitas pessoas aprenderiam a escrever especificações nesta linguagem bem rapidamente

**8)** Eu achei a linguagem muito complicada de escrever

**9)** Eu me senti muito confiante escrevendo especificações nesta linguagem

**10)** Eu preciso aprender muitas coisas antes que eu possa especificar algo com nesta linguagem


## Uso do questionario

Uma explicação das linguagens que serão comparadas deverá ser ministrada. Após esta explicação o questionário deverá ser aplicado. Para cada linguagem as trinta questões da Seção "Novo questionário" deverão ser respondidas. Então o score de cada pesquisa será computado da seguinte forma:

1) Será calculado os scores individuais quanto à linguagem como um todo, quanto a leitura da linguagem e quanto à escrita utiliza a linguagem.

2) O score final de cada linguagem do ponto de vista de um usuário serão quatro notas: as três mencionadas no item 1 e a média dos três valores

3) O score geral final de cada linguagem será calculado com base nas médias das quatro notas dos usuários.


## Referências

(US Gov) https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html Acessado em 02/08/2018