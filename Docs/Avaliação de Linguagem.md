# Avaliação da Linguagem

Segundo (Sebesta R., 2011), uma linguagem de programação pode ser avaliada por quatro critérios:
* Legibilidade
* Facilidade de escrita
* Confiabilidade
* Custo

## Legibilidade

No critério de legibilidade os critérios avalidados são:
* Simplicidade
* Ortogonalidade (Ou capacidade de relacionar elementos primitivos para a criação de elementos complexos)
* Tipo de dados
* Projeto da sintaxe
    * Formato dos indentificadores
    * Palavras especiais
    * Forma e significado


## Facilidade de escrita

No critério de facilidade de escrita os critérios avalidados são:
* Simplicidade
* Ortogonalidade (Ou capacidade de relacionar elementos primitivos para a criação de elementos complexos)
* Suporte a abstração
* Expressividade

## Confiabilidade

No critério de confiabilidade os critérios avalidados são:
* Verificação de tipos
* Tratamento de exceções
* Utilização de apelidos
* Legibilidade e facilidade de escrita

## Custo

No critério de custo os critérios avalidados são:

* Custo de treinamento
* Custo de escrita de programas
* Custo de compilação de programas
* Custo de executar programas escritos
* Custo de implementação da linguagem
* Custo de uma confiabilidade baixa
* Custo de manter programas

## Avaliação de linguangem de especificação
Para avaliar linguagem de especificação será preciso utilizar parâmentros diferentes dos usados para avalição linguagem de programação.

* No critério de **Legibilidade** será utilizado: `Simplicidade` e `Projeto da sintaxe`.

* No critério de **Facilidade de escrita** será utilizado: `Simplicidade`, `Suporte a abstração` e `Expressividade`

* No critério de **Confiabilidade** será utilizado: `Legibilidade` e `Facilidade de escrita`

* No critério de **Custo** será utilizado: `Custo de tereinamento`, `Custo de escrita de programas` e `Custo de manter programas`


## Adequação do SUS (System Usability Model)

 O SUS (US Gov) utiliza um questionário com dez perguntas:

**1)** I think that I would like to use this system frequently.
**2)** I found the system unnecessarily complex.
**3)** I thought the system was easy to use.
**4)** I think that I would need the support of a technical person to be able to use this system.
**5)** I found the various functions in this system were well integrated.
**6)** I thought there was too much inconsistency in this system.
**7)** I would imagine that most people would learn to use this system very quickly.
**8)** I found the system very cumbersome to use.
**9)** I felt very confident using the system.
**10)** I needed to learn a lot of things before I could get going with this system.

Cada pergunta pode ser respondida com um valor entre 1 e 5.

O SUS resulta em um valor único geral para o sistema, mas para analisar oa linguagem será preciso analisar do ponto de vista de leitura de algo especificado e de escrita de uma especificação. Para tanto é possível criar uma variação deste questionário contendo 30 questões, 10 para avaliar a linguagem como um todo, 10 para valiar a escrita de algo, com a linguagem e 10 para avaliar a leitura de algo escrito na linguage. Com estes valores seria possível avaliar o sistema sob três ângulos diferentes.

## Referência
(Sebesta R., 2011) Sebesta R., Conceitos de linguagens de programação Nona Edição, Editora bookman, Páginas 26-38, Capítulo 1 Seção 1.3

(US Gov) https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html Acessado em 02/08/2018