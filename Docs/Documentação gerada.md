# Documentação gerada

O Documento que será gerado pelo interpretador deverá ter

* Capa (Ler de arquivo de configuração)
* Índice (Gerar a partir do conteúdo)
* Descrição do projeto (Ler de arquivo de configuração)
* Dados do projeto 
    * Número de requisitos - (função num_requirements)
    * Número de atores diferentes - (função count_actors)
    * Número de requisitos por ator - (função count_actors)
    * Grafo de PERT-CPM - (função build_pert_cpm) ---- TESTAR -----
    * Equipe de desenvolvimento - (função count_developers)
    * Tempo de trabalho estimado de cada desenvolvedor ---- Talvez não valha a pena implementar
    * Tempo de trabalho estimado para implementar todos os requisitos (função count_number_of_hours_worked_in_project)
    * Número de Sprints - (função get_sprint_information)
    * Número de requisitos por grupo (função count_req_by_group)
* Métricas definidas (Arquivo de métricas)
* Descrição dos requisitos (Cada arquivo de requisitos)

